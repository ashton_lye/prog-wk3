﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            //wk 3 ex 3
            //making array
            var colours = new string[] { "red", "blue", "orange", "white", "black" };

            Console.WriteLine(string.Join(",", colours));

        }
    }
}
