﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            //wk 3 ex 4
            //making array
            var colours = new string[] { "red", "blue", "orange", "white", "black" };

            Array.Sort(colours);
            Console.WriteLine(string.Join(",", colours));

        }
    }
}
