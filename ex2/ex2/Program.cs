﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            //wk 3 ex 2
            //making array
            var colours = new string[] { "red", "blue", "orange", "white", "black" };


            foreach (var x in colours)
            {
                Console.WriteLine(x);
            }
        }
    }
}
